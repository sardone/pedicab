-- SQL dump generated using DBML (dbml-lang.org)
-- Database: PostgreSQL
-- Generated at: 2023-02-23T16:28:24.508Z

CREATE SCHEMA "user";

CREATE SCHEMA "product";

CREATE SCHEMA "customer";

CREATE SCHEMA "transaction";

CREATE TABLE "user"."item" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text UNIQUE NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "product"."item" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "product"."set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "product"."item_set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "product" uuid,
  "set" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "product"."key" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "product"."key_set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "key" uuid,
  "set" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "product"."value" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "product" uuid,
  "set" uuid,
  "key" uuid,
  "value" text,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "customer"."item" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "customer"."set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "customer"."item_set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "customer" uuid,
  "set" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "customer"."key" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "customer"."key_set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "key" uuid,
  "set" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "customer"."value" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "customer" uuid,
  "set" uuid,
  "key" uuid,
  "value" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "transaction"."item" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "customer" uuid,
  "product" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "transaction"."set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "transaction"."item_set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "transaction" uuid,
  "set" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "transaction"."key" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" text NOT NULL,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "transaction"."key_set" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "key" uuid,
  "set" uuid,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

CREATE TABLE "transaction"."value" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "transaction" uuid,
  "set" uuid,
  "key" uuid,
  "value" text,
  "user" uuid,
  "date" timestamptz DEFAULT (current_timestamp(0))
);

ALTER TABLE "user"."item" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "product"."item" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "product"."set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "product"."item_set" ADD FOREIGN KEY ("product") REFERENCES "product"."item" ("id");

ALTER TABLE "product"."item_set" ADD FOREIGN KEY ("set") REFERENCES "product"."set" ("id");

ALTER TABLE "product"."item_set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "product"."key" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "product"."key_set" ADD FOREIGN KEY ("key") REFERENCES "product"."key" ("id");

ALTER TABLE "product"."key_set" ADD FOREIGN KEY ("set") REFERENCES "product"."set" ("id");

ALTER TABLE "product"."key_set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "product"."value" ADD FOREIGN KEY ("product") REFERENCES "product"."item" ("id");

ALTER TABLE "product"."value" ADD FOREIGN KEY ("set") REFERENCES "product"."set" ("id");

ALTER TABLE "product"."value" ADD FOREIGN KEY ("key") REFERENCES "product"."key" ("id");

ALTER TABLE "product"."value" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "customer"."item" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "customer"."set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "customer"."item_set" ADD FOREIGN KEY ("customer") REFERENCES "customer"."item" ("id");

ALTER TABLE "customer"."item_set" ADD FOREIGN KEY ("set") REFERENCES "customer"."set" ("id");

ALTER TABLE "customer"."item_set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "customer"."key" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "customer"."key_set" ADD FOREIGN KEY ("key") REFERENCES "customer"."key" ("id");

ALTER TABLE "customer"."key_set" ADD FOREIGN KEY ("set") REFERENCES "customer"."set" ("id");

ALTER TABLE "customer"."key_set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "customer"."value" ADD FOREIGN KEY ("customer") REFERENCES "customer"."item" ("id");

ALTER TABLE "customer"."value" ADD FOREIGN KEY ("set") REFERENCES "customer"."set" ("id");

ALTER TABLE "customer"."value" ADD FOREIGN KEY ("key") REFERENCES "customer"."key" ("id");

ALTER TABLE "customer"."value" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "transaction"."item" ADD FOREIGN KEY ("customer") REFERENCES "customer"."item" ("id");

ALTER TABLE "transaction"."item" ADD FOREIGN KEY ("product") REFERENCES "product"."item" ("id");

ALTER TABLE "transaction"."item" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "transaction"."set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "transaction"."item_set" ADD FOREIGN KEY ("transaction") REFERENCES "transaction"."item" ("id");

ALTER TABLE "transaction"."item_set" ADD FOREIGN KEY ("set") REFERENCES "transaction"."set" ("id");

ALTER TABLE "transaction"."item_set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "transaction"."key" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "transaction"."key_set" ADD FOREIGN KEY ("key") REFERENCES "transaction"."key" ("id");

ALTER TABLE "transaction"."key_set" ADD FOREIGN KEY ("set") REFERENCES "transaction"."set" ("id");

ALTER TABLE "transaction"."key_set" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");

ALTER TABLE "transaction"."value" ADD FOREIGN KEY ("transaction") REFERENCES "transaction"."item" ("id");

ALTER TABLE "transaction"."value" ADD FOREIGN KEY ("set") REFERENCES "transaction"."set" ("id");

ALTER TABLE "transaction"."value" ADD FOREIGN KEY ("key") REFERENCES "transaction"."key" ("id");

ALTER TABLE "transaction"."value" ADD FOREIGN KEY ("user") REFERENCES "user"."item" ("id");